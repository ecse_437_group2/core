package ca.mcgill.sel.core.weaver;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;

import ca.mcgill.sel.core.CORELink;
import ca.mcgill.sel.core.COREModel;
import ca.mcgill.sel.core.COREModelComposition;
import ca.mcgill.sel.core.COREModelElement;
import ca.mcgill.sel.core.COREModelElementComposition;
import ca.mcgill.sel.core.COREModelExtension;
import ca.mcgill.sel.core.COREModelReuse;
import ca.mcgill.sel.core.COREVisibilityType;
import ca.mcgill.sel.core.CoreFactory;
import ca.mcgill.sel.core.weaver.util.COREModelCompositionUpdater;
import ca.mcgill.sel.core.weaver.util.COREReferenceUpdater;
import ca.mcgill.sel.core.weaver.util.WeaverListener;
import ca.mcgill.sel.core.weaver.util.WeavingInformation;


/**
 * The COREWeaver serves 2 purposes:
 *   - it aggregates weavers for all the languages that are being used within CORE. During startup (or at the latest
 *     whenever a {@link COREModel} is loaded into memory), the corresponding {@link COREModelWeaver} should
 *     register with this class
 *   - it implements all CORE related composition algorithms 
 * 
 * @author Joerg
 */
public final class COREWeaver {
    
    /**
     * File to save an aspect temporarily.
     */
    private static final File TEMPORARY_MODEL_FILE = new File("temp.ram");
    
    /**
     * The singleton instance of this class.
     */
    private static COREWeaver instance;
    
    private Map<Class<? extends COREModel>, COREModelWeaver<? extends COREModel>> weavers = new
            HashMap<Class<? extends COREModel>, COREModelWeaver<? extends COREModel>>();

    private WeavingInformation weavingInformation;
    
    /**
     * Creates a new instance of the weaver.
     */
    private COREWeaver() {
        weavingInformation = new WeavingInformation();
    }

    /**
     * Returns the singleton instance of {@link COREWeaver}.
     * 
     * @return the singleton instance
     */
    public static COREWeaver getInstance() {
        if (instance == null) {
            instance = new COREWeaver();
        }

        return instance;
    }
    
    /**
     * Method to register a specific {@link COREModelWeaver} for models (instances of {@link COREModel}) used
     * within CORE. Every modelling language must register a weaver during startup. From then on, whenever
     * two instances of {@link COREModel} are to be woven, the registered {@link COREModelWeaver} will be
     * called.
     * 
     * @param modelClass the Class instance representing the COREModel
     * @param weaver the instance of {@link COREModelWeaver} to register
     * @param <T> the specific COREModel type
     */
    public <T extends COREModel> void registerCOREModelWeaver(Class<T> modelClass, COREModelWeaver<T> weaver) {
        weavers.put(modelClass, weaver);
    }
    
    /**
     * Weave a single {@link COREModelComposition} into the given {@link COREModel} (could be a reuse or an
     * extension).
     * 
     * @param model - The {@link COREModel} to weave into.
     * @param composition - The {@link COREModelComposition} that refers to the {@link COREModel} that is to
     *        be woven into model.
     * @param listener - The {@link WeaverListener} to report any exceptions to.
     * @param <T> the specific COREModel type
     */
    public <T extends COREModel> void weaveSingle(T model, COREModelComposition composition,
           WeaverListener<T> listener) {
        listener.weavingStarted();
    
        try {
            COREModelWeaver<T> weaver = getModelWeaver(model);
    
            System.out.println("Weaving " + composition.getSource().getName() + " into " + model.getName());
            
            // the following line used to be a call to loadNewInstance... But lets try if it works this way too!
            T modelCopy = EcoreUtil.copy(model);
    
            COREModelComposition compositionCopy = getModelCompositionCopy(model, composition, modelCopy);
            
            T result = null;
    
            // populate weaving information with the information from the COREModelComposition
            weavingInformation.clear();
            populateWeavingInformation(weavingInformation, compositionCopy);
    
            result = weaver.weaveModel(modelCopy, compositionCopy, weavingInformation);
            
            System.out.println("Weaving info after structural weaver: ");
            weavingInformation.print();
            
            
            if (compositionCopy instanceof COREModelReuse) {
                modelCopy.getModelReuses().remove(compositionCopy);
            } else {
                modelCopy.getModelExtensions().remove(compositionCopy);
            }           
                  
            weaveAndUpdateCompositions(modelCopy, composition.getSource(), weavingInformation);
            
            if (result != null) {
                listener.weavingFinished(result);
            } else {
                listener.weavingFailed(new WeaverException("No result was produced."));
            }
            // CHECKSTYLE:IGNORE IllegalCatch: There could be many different exceptions during weaving.
        } catch (Exception e) {
            // TODO: catch in lower level weaver operations, and throw custom exception.
            listener.weavingFailed(new WeaverException(e));
        }
    }

    /**
         * This operation copies the {$link COREModelElement} (which is supposed to be in a source COREModel of the
         * same type as the target model) into the target COREModel into the same container.
         * The operation copies all the COREModelElements contained in modelElement recursively.
         * 
         * @param modelElement the model element that must be copied
         * @param target the COREModel into which the element is to be copied
         * @param info the weaving information
         * @param composition the COREModelComposition for this weaving
         * @param <M> the type of COREModelElement to be used
         * @return the new copy
         */
        public static <M extends COREModelElement> M copyModelElement(M modelElement, COREModel target,
                WeavingInformation info, COREModelComposition composition) {
            M modelElementCopy = EcoreUtil.copy(modelElement);
            
            // perform automated information hiding if the composition is a reuse
            if (composition instanceof COREModelReuse) {
                if (modelElementCopy.getVisibility() == COREVisibilityType.PUBLIC) {
                    modelElementCopy.setVisibility(COREVisibilityType.CONCERN);
                }
            }
            
            // to add the copied model element into the target model at the right place
            // first traverse in the original model from the modelElement up to the root, remembering
            // the containing features
            
    //        ArrayList<EStructuralFeature> featureList = new ArrayList<EStructuralFeature>();
    //        EStructuralFeature containingFeature = modelElement.eContainingFeature();
    //        COREModelElement currentElement = modelElement;
    //        while (currentElement != null) {
    //            featureList.add(containingFeature);
    //            System.out.println("Model element " + currentElement.getName() + " is in feature: " + containingFeature);
    //            if (currentElement.eContainer() instanceof COREModelElement) {
    //                currentElement = (COREModelElement) currentElement.eContainer();
    //                containingFeature = currentElement.eContainingFeature();
    //            } else {
    //                currentElement = null;
    //            }
    //        }
    //        
    //        // now find the spot to insert the modelElementCopy in the target model
    //        int last = featureList.size() - 1;
    //        containingFeature = featureList.get(last);
    //        currentElement = (COREModelElement) target.eGet(containingFeature);
    //        COREModelElement previousElement = currentElement;
    //        while (last > 0) {
    //            System.out.println("Model element: " + currentElement);
    //            last--;
    //            previousElement = currentElement;
    //            currentElement = (COREModelElement) currentElement.eGet(containingFeature);
    //            containingFeature = featureList.get(last);
    //        }
            
            // insert the modelElementCopy into the found container
            //((EList<COREModelElement>) previousElement.eGet(containingFeature)).add(modelElementCopy);
            
            info.add(modelElement, modelElementCopy);
            
            return modelElementCopy;
        }

        
    /**
     * Weave the set of {@link COREModel}s and all their {@link COREModelExtensions} into 
     * a new model.
     * 
     * @param models - The list of {@link COREModel}s to weave.
     * @param listener - The {@link WeaverListener} to report any exceptions to.
     * @param <T> the specific COREModel type
     */
    public <T extends COREModel> void weaveModels(List<T> models, WeaverListener<T> listener) {
        if (models.size() == 0) {
            // TODO: Figure out what to do if this method is called with an empty list of models
            return;
        }
        listener.weavingStarted();

        T result = null;
        
        try {
            COREModelWeaver<T> weaver = getModelWeaver(models.get(0));

            if (models.size() == 1) {
                // we're only weaving one model (and all it's extensions). So let's make a copy first that
                // will then contain the woven result
                result = EcoreUtil.copy(models.get(0));
            } else {
                // create a new empty model
                result = weaver.createEmptyModel();
                
                // loops though all the models and create a COREModelExtension from the new, empty model 
                // to the model that is supposed to be woven
                // Also construct the new name as we go
                String newName = "Woven";
                
                for (T model : models) {
                    COREModelExtension modelExtension = CoreFactory.eINSTANCE.createCOREModelExtension();
                    modelExtension.setSource(model);
                    result.getModelExtensions().add(modelExtension);
                    newName = newName + "_" + model.getName();
                }
                result.setName(newName);
            }
            
            // now loop through all model extensions and weave them successively into the result model
            for (COREModelExtension ext : result.getModelExtensions()) {
                System.out.println("Weaving " + ext.getSource().getName() + " into " + result.getName());
                
                //TODO: Do the actual weaving by rank using weave single
            }
                
            listener.weavingFinished(result);
            // CHECKSTYLE:IGNORE IllegalCatch: There could be many different exceptions during weaving.
        } catch (Exception e) {
            // TODO: catch in lower level weaver operations, and throw custom exception.
            listener.weavingFailed(new WeaverException(e));
        }
    }
        
    /**
     * Method that returns the registered {@link COREModelWeaver} for a given COREModel class.
     * 
     * @param modelClass the Class instance representing the COREModel
     * @param <T> the specific COREModel type
     * 
     * @return the previously registered {@link COREModelWeaver}
     * @throws WeaverException is no weaver is registered for the COREModel class
     */
    private <T extends COREModel> COREModelWeaver<T> getModelWeaver(Class<T> modelClass) throws WeaverException {
        if (!weavers.containsKey(modelClass)) {
            throw new WeaverException(String.format("No Weaver registered for type: %s", modelClass));
        }
        
        @SuppressWarnings("unchecked")
        COREModelWeaver<T> weaver = (COREModelWeaver<T>) weavers.get(modelClass);
        
        return weaver;
    }
    
    /**
     * Method that returns the registered {@link COREModelWeaver} for a given COREModel.
     * 
     * @param model an instance of COREModel that needs to be woven
     * @param <T> the specific COREModel type
     * 
     * @return the previously registered {@link COREModelWeaver}
     * @throws WeaverException is no weaver is registered for the COREModel class
     */
    private <T extends COREModel> COREModelWeaver<T> getModelWeaver(T model) throws WeaverException {
        @SuppressWarnings("unchecked")
        Class<T> modelClass = (Class<T>) model.getClass();
        
        return getModelWeaver(modelClass);
    }
    
    /**
     * This operation goes through all CORELinks that link contains, if any, and adds those also
     * to the wqeaving information
     * 
     * @param info the weaving information that should be populated
     * @param link the link that might contain child links
     */
    private void populateWeavingInformationOfNestedLinks(WeavingInformation info, CORELink<?> link) {
        for (EObject obj : link.eContents()) {
            CORELink<?> nestedLink = (CORELink<?>) obj;
            
            weavingInformation.add(nestedLink.getFrom(), nestedLink.getTo());
            
            // What if the nestedLink has further children?
            populateWeavingInformationOfNestedLinks(info, nestedLink);
        }
    }
    
    /**
     * This operation takes all the composition mappings from the COREModelComposition and stores
     * the mapping in the weaving information object.
     * 
     * @param info the weaving information that should be populated
     * @param composition the COREModelComposition that is going to be woven
     */
    private void populateWeavingInformation(WeavingInformation info, COREModelComposition composition) {
        
        // process the mappings
        for (COREModelElementComposition<?> mec : composition.getCompositions()) {
            
            // Currently we can only handle CORELinks... If ever we support patterns in the future, we need
            // to figure out how we want to populate the weaving info correctl based on the pattern
            CORELink<?> link = (CORELink<?>) mec;
                        
            weavingInformation.add(link.getFrom(), link.getTo());
            
            // If the link contains other links (such as for example ClassifierMappings, which contain
            // attribute mappings and operation mappings, then we should also take care of those mappings

            populateWeavingInformationOfNestedLinks(info, link);
        }
    }
    
    
    /**
    * If modelCopy is a copy of originalModel, then this operation finds the {@link COREModelComposition}
    * in modelCopy that corresponds to originalComposition.
    * 
    * @param originalModel - The model to weave in.
    * @param originalCompositon - The original {@link COREModelComposition}.
    * @param modelCopy - The copy of the originalModel.
    * @return the found {link COREModelComposition}.
    */
    private COREModelComposition getModelCompositionCopy(COREModel originalModel,
           COREModelComposition originalCompositon, COREModel modelCopy) {
        EStructuralFeature feature = originalCompositon.eContainingFeature();
        
        @SuppressWarnings("unchecked")
        EList<? extends COREModelComposition> compositions = 
                (EList<? extends COREModelComposition>) originalModel.eGet(feature);
        @SuppressWarnings("unchecked")
        EList<? extends COREModelComposition> compositionsCopy = 
                (EList<? extends COREModelComposition>) modelCopy.eGet(feature);
        
        int index = compositions.indexOf(originalCompositon);
        COREModelComposition compositionCopy = compositionsCopy.get(index);
        
        return compositionCopy;
    }
//    
//    public <T extends COREModel> void weaveAll(T model, WeaverListener<T> listener) {
//        
//    }
//    
//    /**
//     * Weaves the aspects of the selected features of the given concern to one aspect.
//     * 
//     * @param concern - the reused concern
//     * @param configuration - the configuration containing the selection that was made.
//     * @param saveModel - Whether to save the woven result in a file or not.
//     * @param weaveAll - Whether to weave state and AspectMessageViews as well.
//     * @return the woven aspect containing the merge result of all selected features
//     * @throws WeaverException 
//     */
//    public <T extends COREModel> void weaveSelectedFeatures(COREConcern concern, COREReuseConfiguration configuration, Class<T> modelClass, boolean saveModel,
//            boolean weaveAll) throws WeaverException {
//        
//    }
//    
//    private <T extends COREModel> T createModel(Class<T> modelClass, COREConcern concern, Set<COREModel> models) throws WeaverException {
//        COREModelWeaver<T> weaver = getModelWeaver(modelClass);
//        T emptyModel = weaver.createEmptyModel();
//        
//        for (COREModel model : models) {
//            COREModelExtension extension = CoreFactory.eINSTANCE.createCOREModelExtension();
//            extension.setSource(model);
//            
//            emptyModel.getModelExtensions().add(extension);
//        }
//        
//        return emptyModel;
//    }
//    
    /**
     * Loads a new instance of a model. 
     * This is required, because lower-level model
     * that contain dependencies will be modified, i.e., it's dependencies are
     * woven into it. If this (lower-level) model is loaded somewhere else, e.g., a GUI,
     * the modifications are reflected there, which is unwanted behavior.
     * If the model were cloned, it would require the mappings of the instantiation
     * for this lower-level model to be updated to the new elements, which would
     * be more time-consuming to do.
     * Therefore, to circumvent this, the model is saved in a temporary file
     * and loaded again using a separate resource set, which forces to load
     * other models to be loaded as new instances. Otherwise the existing
     * resource set would get the resources for the lower-level model
     * from its cache.
     * 
     * @param model the model to load a new instance for
     * @return a new instance of the given model
     * @param <T> the specific COREModel type
     */
    private <T extends COREModel> T loadNewInstance(T model) {
        // The given COREModel has to be cloned. Otherwise, when adding the model to the new resource
        // it gets removed from its current resource. This will mean that the given model
        // is directly modified.
        T result = EcoreUtil.copy(model);

        String pathName = TEMPORARY_MODEL_FILE.getAbsolutePath();

        // Use our own resource set for saving and loading to workaround the issue.
        ResourceSet resourceSet = new ResourceSetImpl();

        // Create a resource to temporarily save the aspect.
        Resource resource = resourceSet.createResource(URI.createFileURI(pathName));
        resource.getContents().add(result);

        try {
            resource.save(Collections.EMPTY_MAP);

            // Load the temporary aspect ...
            resource = resourceSet.getResource(URI.createFileURI(pathName), true);
            @SuppressWarnings("unchecked")
            T newCopy = (T) resource.getContents().get(0);

            // Copy the model to loose the reference to the temporary file
            result = EcoreUtil.copy(newCopy);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Delete the temporary file ...
        TEMPORARY_MODEL_FILE.delete();

        return result;
    }
    
    /**
     * This method weaves the model compositions by iterating through
     * all compositions in the source model and copies them into the base model,
     * and then updates the model element references to refer to the local elements.
     *
     * @param base The COREModel that has been woven into
     * @param source The COREModel that was woven
     * @param weavingInformation The data structure containing all mapping information
     */
    private static void weaveAndUpdateCompositions(COREModel base, COREModel source,
            WeavingInformation weavingInformation) {
        
        // If the source model has compositions it is necessary to copy the compositions to the base model
        for (COREModelExtension externalExtension : source.getModelExtensions()) {
            COREModelExtension extensionCopy = EcoreUtil.copy(externalExtension);
            base.getModelExtensions().add(extensionCopy);
        }
        for (COREModelReuse externalModelReuses : source.getModelReuses()) {
            COREModelReuse reuseCopy = EcoreUtil.copy(externalModelReuses);
            base.getModelReuses().add(reuseCopy);
        }

        // Then update any references in the mappings that still refer to the source model to now
        // refer to the correct elements in the base model
        COREReferenceUpdater updater = COREModelCompositionUpdater.getInstance();
        updater.update(base.getModelExtensions(), weavingInformation);
        updater.update(base.getModelReuses(), weavingInformation);
    }


}

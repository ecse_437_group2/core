package ca.mcgill.sel.core.weaver.util;

import java.util.List;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.Switch;

/**
 * Takes care of updating the references of elements of any {@link EObject} of any model
 * on the {@link WeavingInformation} provided by the weaver.
 * This class is abstract but its subclass should be defined as singleton.
 *
 * @author mschoettle
 */
public abstract class COREReferenceUpdater {

    /**
     * The {@link WeavingInformation} used to update the references.
     */
    protected WeavingInformation currentWeavingInformation;
    /**
     * The {@link Switch} defining how to update.
     */
    protected Switch<?> switcher;
    
    /**
     * Constructor for a {@link COREReferenceUpdater}. Initializes the {@link Switch} that will be used for updating.
     */
    public COREReferenceUpdater() {
        this.switcher = createSwitcher();
    }

    /**
     * Creates a new switcher instance. Is called in the constructor.
     * 
     * @return the {@link Switch} to be used by the {@link COREReferenceUpdater} should be returned here.
     */
    protected abstract Switch<?> createSwitcher();

    /**
     * Updates an {@link EObject} based on the given {@link WeavingInformation}.
     * Also updates all objects in the hierarchy of the given object.
     * 
     * @param eObject the object to update
     * @param weavingInformation the weaving information
     */
    public void update(EObject eObject, WeavingInformation weavingInformation) {
        this.currentWeavingInformation = weavingInformation;

        // Update the object itself.
        switcher.doSwitch(eObject);

        // Then update the whole containment hierarchy of the object.
        TreeIterator<EObject> iterator = EcoreUtil.getAllContents(eObject, true);

        while (iterator.hasNext()) {
            update(iterator.next(), weavingInformation);
        }
    }

    /**
     * Updates a list of {@link EObject} based on the given {@link WeavingInformation}.
     * Also updates all objects in the hierarchy of the given object.
     * 
     * @param eObjects the list of objects to update
     * @param weavingInformation the weaving information
     * @see #update(EObject, WeavingInformation)
     */
    public void update(List<? extends EObject> eObjects, WeavingInformation weavingInformation) {
        for (EObject eObject : eObjects) {
            update(eObject, weavingInformation);
        }
    }

}

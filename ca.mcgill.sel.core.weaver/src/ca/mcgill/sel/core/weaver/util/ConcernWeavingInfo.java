package ca.mcgill.sel.core.weaver.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.ecore.EClassifier;

import ca.mcgill.sel.core.COREFeature;
import ca.mcgill.sel.core.COREModel;
import ca.mcgill.sel.core.COREModelExtension;
import ca.mcgill.sel.core.COREReuse;
import ca.mcgill.sel.core.COREReuseConfiguration;
import ca.mcgill.sel.core.util.COREConfigurationUtil;
import ca.mcgill.sel.core.util.COREModelUtil;

/**
 * Stores information about the current dependency level for each of external aspect.
 * This class has methods that allows the RAMWeaver to weave the instantiation
 * starting with the external aspect that has the highest depth rank in the
 * dependency tree.
 * 
 * @author oalam
 * @author CCamillieri
 */
public class ConcernWeavingInfo {

    // Used to weave extends
    private static int maxDepth;
    private COREModelExtension modelExtension;
    private HashMap<COREModelExtension, Integer> ranks;
    private List<COREModelExtension> instantiations;

    // Keep track of the Aspects that need to be woven for this concern. Used to name woven aspects.
    private List<COREModel> wovenAspects;

    // keep hierarchy of weavingInformation
    private ConcernWeavingInfo parentWeavingInfo;
    private Map<COREReuse, ConcernWeavingInfo> childrenWeavingInfo;

    /**
     * The constructor.
     */
    public ConcernWeavingInfo() {
        this.ranks = new HashMap<>();
        this.instantiations = new ArrayList<>();
        this.wovenAspects = new ArrayList<>();
        this.childrenWeavingInfo = new HashMap<COREReuse, ConcernWeavingInfo>();
    }

    /**
     * Constructor for {@link ConcernWeavingInfo}.
     * Creates nested {@link ConcernWeavingInfo}s with extendingConfigurations from the the given configuration.
     *
     * @param configuration - configuration containing information about selected features.
     */
    public ConcernWeavingInfo(COREReuseConfiguration configuration, EClassifier classifier) {
        this();
        if (configuration != null) {
            // Get aspects to weave
            Set<COREFeature> selectedFeatures = new HashSet<COREFeature>(configuration.getSelected());
            Set<COREModel> resolvedAspects =
                    COREModelUtil.getRealizationModels(selectedFeatures, classifier);

            if (resolvedAspects != null) {
                this.wovenAspects.addAll(resolvedAspects);
            }

            for (COREReuseConfiguration extConfig : configuration.getExtendingConfigurations()) {
                COREReuseConfiguration baseConfig = extConfig.getReuse().getSelectedConfiguration();
                COREConfigurationUtil.mergeConfigurations(extConfig, baseConfig, true, false);
                
                ConcernWeavingInfo child = new ConcernWeavingInfo(extConfig, classifier);
                child.setParentWeavingInfo(this);
                
                this.childrenWeavingInfo.put(extConfig.getReuse(), child);
            }
        }
    }

    /**
     * Get the list of aspects woven for this given concern.
     *
     * @return the woven aspects.
     */
    public List<COREModel> getWovenAspects() {
        return wovenAspects;
    }
    
    /**
     * Get the children {@link ConcernWeavingInfo}.
     *
     * @return the children {@link ConcernWeavingInfo}.
     */
    public Map<COREReuse, ConcernWeavingInfo> getChildrenWeavingInfo() {
        return childrenWeavingInfo;
    }

    /**
     * Get the parent {@link ConcernWeavingInfo}.
     *
     * @return the parent {@link ConcernWeavingInfo}.
     */
    public ConcernWeavingInfo getParentWeavingInfo() {
        return parentWeavingInfo;
    }

    /**
     * Set the parent {@link ConcernWeavingInfo}.
     *
     * @param parent - the parent {@link ConcernWeavingInfo}.
     */
    public void setParentWeavingInfo(ConcernWeavingInfo parent) {
        this.parentWeavingInfo = parent;
    }

    /**
     * Returns the highest ranked instantiation.
     *
     * @return highest ranked instantiation.
     */
    public COREModelExtension getHighestRankedAspect() {
        return modelExtension;
    }

    /**
     * Returns the highest rank.
     *
     * @param base the input aspect.
     * @return aspect the aspect to display
     */
    public int getHighestRank(COREModel base) {
        instantiations.clear();
        ranks.clear();
        instantiations.addAll(base.getModelExtensions());
        getMaxDepth(base, 0);
        int highestr = -1;
        for (COREModelExtension inst : ranks.keySet()) {
            if (ranks.get(inst) > highestr) {
                highestr = ranks.get(inst);
                modelExtension = inst;
            }
        }
        return highestr;
    }

    /**
     * Returns the maximum depth for a given aspect at a given depth.
     *
     * @param aspect the input aspect.
     * @param depth the given depth of the aspect.
     */
    private void getMaxDepth(COREModel aspect, int depth) {
        List<COREModelExtension> insts = aspect.getModelExtensions();
        for (COREModelExtension ins : insts) {
            maxDepth = -1;
            getInstantiationDepth(1, ins.getSource());
            ranks.put(ins, maxDepth);
        }
    }

    /**
     * Returns the maximum depth of an instantiation for a given aspect at a given depth.
     *
     * @param external the input aspect.
     * @param depth the given depth of the aspect.
     */
    private void getInstantiationDepth(int depth, COREModel external) {
        for (COREModelExtension ins : external.getModelExtensions()) {
            COREModel externalAspect = ins.getSource();
            getInstantiationDepth(depth + 1, externalAspect);
        }
        if (depth > maxDepth) {
            maxDepth = depth;
        }
    }

}
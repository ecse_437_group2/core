package ca.mcgill.sel.commons.emf.util;

import org.eclipse.emf.common.command.BasicCommandStack;

/**
 * Same as EMF {@link BasicCommandStack} except the listeners are notified on save (when saveisDone() is called).
 * 
 * @author CCamillieri
 */
public class CORECommandStack extends BasicCommandStack {

    @Override
    public void saveIsDone() {
        super.saveIsDone();
        notifyListeners();
    }
    
    /**
     * Returns whether it is possible to revert to the last saved state.
     * A revert is not possible if the user performs the following scenario:
     * 
     * <ol>
     *   <li>Make changes</li>
     *   <li>Save</li>
     *   <li>Undo</li>
     *   <li>Make changes</li>
     * </ol>
     * 
     * I.e., if the user undoes after saving and then makes changes, the undone command is gone and can not be redone.
     * 
     * @return true, if the stack can be reverted to the last saved state, false otherwise
     */
    public boolean canRevertToLastSave() {
        // CHECKSTYLE:IGNORE MagicNumber: This is an EMF constant that deals with exactly this problem.
        return this.saveIndex != -2;
    }

    /**
     * Undo or redo commandStack to go back to previous save.
     * A user must make sure beforehand that this is possible.
     * 
     * @see #canRevertToLastSave()
     */
    public void goToLastSave() {
        if (!canRevertToLastSave()) {
            throw new IllegalStateException("Not possible to revert back to last save.");
        }
        
        boolean undo = this.top > this.saveIndex;
        while (isSaveNeeded()) {
            if (undo) {
                undo();
            } else {
                redo();
            }
        }
    }

}

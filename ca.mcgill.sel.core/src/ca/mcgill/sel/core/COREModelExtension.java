/**
 */
package ca.mcgill.sel.core;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>CORE Model Extension</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ca.mcgill.sel.core.CorePackage#getCOREModelExtension()
 * @model
 * @generated
 */
public interface COREModelExtension extends COREModelComposition {
} // COREModelExtension

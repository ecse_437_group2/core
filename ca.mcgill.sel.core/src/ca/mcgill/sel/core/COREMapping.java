/**
 */
package ca.mcgill.sel.core;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>CORE Mapping</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ca.mcgill.sel.core.CorePackage#getCOREMapping()
 * @model
 * @generated
 */
public interface COREMapping<T extends COREModelElement> extends CORELink<T> {

} // COREMapping

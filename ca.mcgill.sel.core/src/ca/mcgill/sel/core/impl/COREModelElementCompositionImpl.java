/**
 */
package ca.mcgill.sel.core.impl;

import ca.mcgill.sel.core.COREModelElement;
import ca.mcgill.sel.core.COREModelElementComposition;
import ca.mcgill.sel.core.CorePackage;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>CORE Model Element Composition</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class COREModelElementCompositionImpl<T extends COREModelElement> extends EObjectImpl implements COREModelElementComposition<T> {
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected COREModelElementCompositionImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return CorePackage.Literals.CORE_MODEL_ELEMENT_COMPOSITION;
    }

} //COREModelElementCompositionImpl

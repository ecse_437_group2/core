/**
 */
package ca.mcgill.sel.core.impl;

import org.eclipse.emf.ecore.EClass;

import ca.mcgill.sel.core.COREModelElement;
import ca.mcgill.sel.core.COREPattern;
import ca.mcgill.sel.core.CorePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>CORE Pattern</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class COREPatternImpl extends COREModelElementCompositionImpl<COREModelElement> implements COREPattern {
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected COREPatternImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return CorePackage.Literals.CORE_PATTERN;
    }

} //COREPatternImpl

/**
 */
package ca.mcgill.sel.core.impl;

import ca.mcgill.sel.core.COREMapping;
import ca.mcgill.sel.core.COREModelElement;
import ca.mcgill.sel.core.CorePackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>CORE Mapping</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class COREMappingImpl<T extends COREModelElement> extends CORELinkImpl<T> implements COREMapping<T> {
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected COREMappingImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return CorePackage.Literals.CORE_MAPPING;
    }
    
    @Override
    public void setTo(T newTo) {
        T oldTo = to;

        super.setTo(newTo);
        
        // Notify new "to" element
        if (newTo != null) {
            InternalEObject toElement = (InternalEObject) newTo;
            newTo.eNotify(new ENotificationImpl(toElement, Notification.SET,
                    CorePackage.Literals.CORE_MODEL_ELEMENT__REFERENCE, false, false));
        }
        // Notify old "to" element
        if (oldTo != null) {
            InternalEObject toElement = (InternalEObject) oldTo;
            oldTo.eNotify(new ENotificationImpl(toElement, Notification.SET,
                    CorePackage.Literals.CORE_MODEL_ELEMENT__REFERENCE, false, false));
        }

    }


} //COREMappingImpl

/**
 */
package ca.mcgill.sel.core.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

import ca.mcgill.sel.core.COREConfiguration;
import ca.mcgill.sel.core.COREFeature;
import ca.mcgill.sel.core.CORENamedElement;
import ca.mcgill.sel.core.COREReuseConfiguration;
import ca.mcgill.sel.core.CorePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>CORE Configuration</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.core.impl.COREConfigurationImpl#getName <em>Name</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.impl.COREConfigurationImpl#getSelected <em>Selected</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.impl.COREConfigurationImpl#getReexposed <em>Reexposed</em>}</li>
 *   <li>{@link ca.mcgill.sel.core.impl.COREConfigurationImpl#getExtendingConfigurations <em>Extending Configurations</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class COREConfigurationImpl extends COREModelCompositionImpl implements COREConfiguration {
    /**
     * The default value of the '{@link #getName() <em>Name</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getName()
     * @generated
     * @ordered
     */
    protected static final String NAME_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getName()
     * @generated
     * @ordered
     */
    protected String name = NAME_EDEFAULT;

    /**
     * The cached value of the '{@link #getSelected() <em>Selected</em>}' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getSelected()
     * @generated
     * @ordered
     */
    protected EList<COREFeature> selected;

    /**
     * The cached value of the '{@link #getReexposed() <em>Reexposed</em>}' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getReexposed()
     * @generated
     * @ordered
     */
    protected EList<COREFeature> reexposed;

    /**
     * The cached value of the '{@link #getExtendingConfigurations() <em>Extending Configurations</em>}' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getExtendingConfigurations()
     * @generated
     * @ordered
     */
    protected EList<COREReuseConfiguration> extendingConfigurations;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected COREConfigurationImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return CorePackage.Literals.CORE_CONFIGURATION;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public String getName() {
        return name;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public void setName(String newName) {
        String oldName = name;
        name = newName;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.CORE_CONFIGURATION__NAME, oldName, name));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EList<COREFeature> getSelected() {
        if (selected == null) {
            selected = new EObjectResolvingEList<COREFeature>(COREFeature.class, this, CorePackage.CORE_CONFIGURATION__SELECTED);
        }
        return selected;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EList<COREFeature> getReexposed() {
        if (reexposed == null) {
            reexposed = new EObjectResolvingEList<COREFeature>(COREFeature.class, this, CorePackage.CORE_CONFIGURATION__REEXPOSED);
        }
        return reexposed;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EList<COREReuseConfiguration> getExtendingConfigurations() {
        if (extendingConfigurations == null) {
            extendingConfigurations = new EObjectContainmentEList<COREReuseConfiguration>(COREReuseConfiguration.class, this, CorePackage.CORE_CONFIGURATION__EXTENDING_CONFIGURATIONS);
        }
        return extendingConfigurations;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case CorePackage.CORE_CONFIGURATION__EXTENDING_CONFIGURATIONS:
                return ((InternalEList<?>)getExtendingConfigurations()).basicRemove(otherEnd, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case CorePackage.CORE_CONFIGURATION__NAME:
                return getName();
            case CorePackage.CORE_CONFIGURATION__SELECTED:
                return getSelected();
            case CorePackage.CORE_CONFIGURATION__REEXPOSED:
                return getReexposed();
            case CorePackage.CORE_CONFIGURATION__EXTENDING_CONFIGURATIONS:
                return getExtendingConfigurations();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @SuppressWarnings("unchecked")
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case CorePackage.CORE_CONFIGURATION__NAME:
                setName((String)newValue);
                return;
            case CorePackage.CORE_CONFIGURATION__SELECTED:
                getSelected().clear();
                getSelected().addAll((Collection<? extends COREFeature>)newValue);
                return;
            case CorePackage.CORE_CONFIGURATION__REEXPOSED:
                getReexposed().clear();
                getReexposed().addAll((Collection<? extends COREFeature>)newValue);
                return;
            case CorePackage.CORE_CONFIGURATION__EXTENDING_CONFIGURATIONS:
                getExtendingConfigurations().clear();
                getExtendingConfigurations().addAll((Collection<? extends COREReuseConfiguration>)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case CorePackage.CORE_CONFIGURATION__NAME:
                setName(NAME_EDEFAULT);
                return;
            case CorePackage.CORE_CONFIGURATION__SELECTED:
                getSelected().clear();
                return;
            case CorePackage.CORE_CONFIGURATION__REEXPOSED:
                getReexposed().clear();
                return;
            case CorePackage.CORE_CONFIGURATION__EXTENDING_CONFIGURATIONS:
                getExtendingConfigurations().clear();
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case CorePackage.CORE_CONFIGURATION__NAME:
                return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
            case CorePackage.CORE_CONFIGURATION__SELECTED:
                return selected != null && !selected.isEmpty();
            case CorePackage.CORE_CONFIGURATION__REEXPOSED:
                return reexposed != null && !reexposed.isEmpty();
            case CorePackage.CORE_CONFIGURATION__EXTENDING_CONFIGURATIONS:
                return extendingConfigurations != null && !extendingConfigurations.isEmpty();
        }
        return super.eIsSet(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
        if (baseClass == CORENamedElement.class) {
            switch (derivedFeatureID) {
                case CorePackage.CORE_CONFIGURATION__NAME: return CorePackage.CORE_NAMED_ELEMENT__NAME;
                default: return -1;
            }
        }
        return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
        if (baseClass == CORENamedElement.class) {
            switch (baseFeatureID) {
                case CorePackage.CORE_NAMED_ELEMENT__NAME: return CorePackage.CORE_CONFIGURATION__NAME;
                default: return -1;
            }
        }
        return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuffer result = new StringBuffer(super.toString());
        result.append(" (name: ");
        result.append(name);
        result.append(')');
        return result.toString();
    }

} //COREConfigurationImpl

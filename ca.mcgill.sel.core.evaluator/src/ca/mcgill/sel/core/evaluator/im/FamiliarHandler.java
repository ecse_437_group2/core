package ca.mcgill.sel.core.evaluator.im;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.common.util.EList;

import ca.mcgill.sel.core.COREFeature;
import ca.mcgill.sel.core.COREFeatureModel;
import ca.mcgill.sel.core.COREFeatureRelationshipType;
import fr.unice.deptinfo.familiar_interpreter.FMEngineException;
import fr.unice.deptinfo.familiar_interpreter.impl.FamiliarInterpreter;
import fr.unice.polytech.modalis.familiar.interpreter.VariableNotExistingException;
import fr.unice.polytech.modalis.familiar.parser.VariableAmbigousConflictException;

/**
 * FamiliarHandler is used to reach familiar and check if a feature configuration is valid.
 * @author berkduran
 *
 */
public class FamiliarHandler {

    private static int totalCount;
    private static int familiarCallCount;
    private FamiliarInterpreter familiarInterpreter;
    private String featureModelDef;
    private String fmName = "fm1";
    private String configName = "config";
    private int configIndex;
    private COREFeature rootFeature;
    private Map<COREFeature, Integer> featureLevels = new HashMap<>();

    /**
     * Constructor also makes the initialization.
     * @param featureModel is the input feature model on which the configurations will be checked.
     */
    public FamiliarHandler(COREFeatureModel featureModel) {
        rootFeature = featureModel.getRoot();
        // prepare the map that contains features and their levels in the model. 
        // this is done once during initialization to save time for each XOR check in checkForXORConflicts()
        for (COREFeature feature : featureModel.getFeatures()) {
            int level = 0;
            COREFeature parent = feature.getParent();
            while (!(parent == null)) {
                level++;
                parent = parent.getParent();
            }
            featureLevels.put(feature, level);
        }
        
        String featureModelText = convertFeatureModelIntoString(rootFeature);
        if (featureModelText.isEmpty()) {
            featureModelText += arrangeFeatureName(rootFeature) + ";";
        }

        //now add the constraints.
        String constraints = getConstraintsFromFeatureModel(featureModel);
        featureModelText += constraints;

        featureModelDef = " = FM(" + featureModelText + ")";            
        //System.out.println("Parsed: " + featureModelDef);
        
        //Sample feature model definition.
        //        featureModelDef = " = FM(OrderProcessing :  Transaction [Approval] [Basket] Fulfillment ; "
        //                + "Transaction :  Payment Tax Invoice [ShipmentCost] ; "
        //                + "Payment :  ( Bill|PayOnDelivery|FrequentFlyer|CreditCard )+ ; "
        //                + "Invoice :  ( Online|Printed ) ; Fulfillment :  ( Shipping|ElectronicDelivery ) ; "
        //                + "Shipping :  [PackageTrackingNo] PackageSlip ; "
        //                + "(PackageTrackingNo->!PayOnDelivery); "
        //                + "(Bill->!PayOnDelivery); "
        //                + "(Bill->!PackageTrackingNo);)";
        
        //System.out.println("Used: " + featureModelDef);

        initialize();
    }

    /** 
     * Initializes familiar for checks by giving it the feature model as input. 
     * After the initialization, familiar interpreter could be used at any time by giving configurations.
     */
    void initialize() {
        // get the familiar interpreter singleton
        // temporarily set output to an empty stream in order to disable familiar's initial printouts.
        PrintStream out = System.out;
        System.setOut(new PrintStream(new OutputStream() {
            @Override public void write(int b) throws IOException { }
        }));
        familiarInterpreter = FamiliarInterpreter.getInstance();
        familiarInterpreter.setVerbose(false); 
        System.setOut(out);
        
        String featureModelString = fmName + featureModelDef;
                
        try {
            familiarInterpreter.eval(featureModelString);
            //FeatureModelVariable fmv;
            //fmv = familiarInterpreter.getFMVariable(fmName);
            familiarInterpreter.getFMVariable(fmName);
            //            System.out.println("Instantiated FM : " + fmv.getSyntacticalRepresentation());
            //familiarInterpreter.eval(configName + " = configuration " + fmName); 

        } catch (FMEngineException e) {
            e.printStackTrace();
        } catch (VariableNotExistingException e) {
            e.printStackTrace();
        } catch (VariableAmbigousConflictException e) {
            e.printStackTrace();
        }
    }

    /**
     * Prepares and returns the text containing the constraints in the feature model.
     * @param featureModel is the input feature model.
     * @return the string describing all the constraints.
     */
    public String getConstraintsFromFeatureModel(COREFeatureModel featureModel) {
        //(Feature1->!Feature2); or (Feature1->Feature2);
        String constraints = "";
        for (COREFeature feature : featureModel.getFeatures()) {
            for (COREFeature excludes : feature.getExcludes()) {
                constraints += "(" + arrangeFeatureName(feature) + "->!" + arrangeFeatureName(excludes) + "); ";
            }
            for (COREFeature requires : feature.getRequires()) {
                constraints += "(" + arrangeFeatureName(feature) + "->" + arrangeFeatureName(requires) + "); ";
            }
        }
        return constraints;
    }

    /**
     * Recursively parses the feature model to arrange the format to give it to Familiar as input.
     * @param feature is the feature to be converted to string.
     * @return string representation of the feature model.
     */
    public String convertFeatureModelIntoString(COREFeature feature) {
        String returnString = "";
        EList<COREFeature> children = feature.getChildren();
        if (children.size() == 0) {
            returnString = "";
        } else {
            returnString = arrangeFeatureName(feature) + " : ";
            if (ImpactModelEvaluationUtil.getRelationshipWithChildren(feature) == null) {
                for (COREFeature child : children) {
                    if (child.getParentRelationship() == COREFeatureRelationshipType.OPTIONAL) {
                        returnString = returnString + " [" + arrangeFeatureName(child) + "]";
                    } else {
                        returnString = returnString + " " + arrangeFeatureName(child);
                    }
                }
                returnString = returnString + " ; ";
                for (COREFeature child : children) {
                    returnString = returnString + convertFeatureModelIntoString(child);
                }
            } else if (ImpactModelEvaluationUtil.getRelationshipWithChildren(feature) 
                    == COREFeatureRelationshipType.XOR) {
                returnString = returnString + " ( ";
                for (COREFeature child : children) {
                    returnString = returnString  + arrangeFeatureName(child) + "|";
                }
                returnString = returnString.substring(0, returnString.length() - 1);
                returnString = returnString + " ) ";
                returnString = returnString + "; ";
                for (COREFeature child : children) {
                    returnString = returnString + convertFeatureModelIntoString(child);
                }
            } else if (ImpactModelEvaluationUtil.getRelationshipWithChildren(feature) 
                    == COREFeatureRelationshipType.OR) {
                returnString = returnString + " ( ";
                for (COREFeature child : children) {
                    returnString = returnString  + arrangeFeatureName(child) + "|";
                }
                returnString = returnString.substring(0, returnString.length() - 1);
                returnString = returnString + " )+ ";
                returnString = returnString + "; ";
                for (COREFeature child : children) {
                    returnString = returnString + convertFeatureModelIntoString(child);
                }
            }
        }
        return returnString;
    }

    /**
     * Checks the feature name and adjusts its name if necessary.
     * @param feature is the feature whose name is to be checked.
     * @return the adjusted feature name, covered with "" if it contains white space.
     */
    private String arrangeFeatureName(COREFeature feature) {
        String featureName = "\"" + feature.getName() + "\"";
        return featureName;
    }

    /**
     * Check whether the given String contains any whitespace characters.
     * @param str the String to check (may be <code>null</code>)
     * @return <code>true</code> if the String is not empty and
     * contains at least 1 whitespace character
     * @see java.lang.Character#isWhitespace
     * @author Rod Johnson
     * @author Juergen Hoeller
     * @author Keith Donald
     * @author Rob Harrop
     * @author Rick Evans
     * @author Jacky.Song
     * @since 16 April 2001
     */
    protected boolean containsWhitespace(String str) {
        if (!(str != null && str.length() > 0)) {
            return false;
        }
        int strLen = str.length();
        for (int i = 0; i < strLen; i++) {
            if (Character.isWhitespace(str.charAt(i))) {
                return true;
            }
        }
        return false;
    }

    /** 
     * Given a feature configuration, checks its validity with Familiar.
     * @param configuration is a configuration to be questioned.
     * @param unwanted is the list of features not wanted in the configuration.
     * @return true if configuration is valid, false if not.
     */
    public boolean checkConfigurationValidity(Set<COREFeature> configuration, Set<COREFeature> unwanted) {
        totalCount++;

        boolean isValid = false;

        //make sure the configuration contains at least the root feature.
        if (!configuration.contains(rootFeature)) {
            configuration.add(rootFeature);
        }
        
        //Check if any of the XOR sibling features are in the configuration.
        if (checkForXORConflicts(new HashSet<COREFeature>(configuration))) {
            //System.out.println("XOR conflicts in config: " + arrangeFeatureConfigurationString(configuration));
            return false;
        }
        
        //Check if any of the unwanted features are in the configuration.
        for (COREFeature featureInConfig : configuration) {
            if (unwanted.contains(featureInConfig)) {
//                System.out.println("Unwanted: " + arrangeFeatureConfigurationString(unwanted) 
//                        + " contains feature " + featureInConfig.getName() + " in config: " 
//                        + arrangeFeatureConfigurationString(configuration));
                return false;
            }
        }

        String featureConfiguration = arrangeFeatureConfigurationString(configuration);
        String unwantedFeatureString = arrangeFeatureConfigurationString(unwanted);
        //System.out.println("Calling Familiar with config: " + featureConfiguration 
        //        + " unwanted: " + unwantedFeatureString);
        // check with Familiar
        // create a configuration of the FM (so that we can apply select operation on it)
        // NB: a familiar configuration is always a configuration of a specific FM  
        try {
            familiarCallCount++;
            String newConfigName = configName + (configIndex++);
            familiarInterpreter.eval(newConfigName + " = configuration " + fmName);
            if (unwantedFeatureString.length() > 0) {
                familiarInterpreter.eval("deselect " + unwantedFeatureString + " in " + newConfigName);
            }
            familiarInterpreter.eval("select " + featureConfiguration + " in " + newConfigName);
            // displaying the "result" of the selection
            Collection<String> selectedFeatures = familiarInterpreter.getSelectedFeature(newConfigName);
            Collection<String> deselectedFeatures = familiarInterpreter.getDeselectedFeature(newConfigName);
            //    configSize = selectedFeatures.size();
            //    System.out.println("Number of selected features : " + configSize);
            //System.out.println("Selected features : " + selectedFeatures);
            //System.out.println("Deselected features : " + deselectedFeatures);
            //System.out.println("Unselected features : " + familiarInterpreter.getUnselectedFeature(newConfigName));
            // checking the configuration for validity and completeness
            //System.out.println("Is the configuration valid (always true with select command)? "
            //       + familiarInterpreter.getConfigurationVariable(newConfigName).isValid());                    
            //System.out.println("Is the configuration complete? " 
            //        + familiarInterpreter.getConfigurationVariable(newConfigName).isComplete());
            if (selectedFeatures.containsAll(parseFeatureConfig(featureConfiguration))) {
                isValid = true;
                //now check if any of the unwanted features is in the configuration.
                if (unwanted.size() > 0) {
                    //String unwantedFeatureString = arrangeFeatureConfigurationString(unwanted);
                    Collection<String> unwantedFeatureList = parseFeatureConfig(unwantedFeatureString);
                    
                    for (String unwantedFeature : unwantedFeatureList) {
                        if (selectedFeatures.contains(unwantedFeature)) {
                            isValid = false;
                            //System.out.println("UNWANTED " + unwantedFeature + " is among selected features!");
                            //System.out.println("Configuration cannot be valid!");
                        }
                    }
                    
                    if (!deselectedFeatures.containsAll(unwantedFeatureList)) {
                        isValid = false;
                        //System.out.println("Some unwanted feature is not among deselected features!");
                        //System.out.println("Configuration cannot be valid!");
                    }
                }
            } 
        } catch (FMEngineException e) {
            e.printStackTrace();
        }
        return isValid;
    }

    /** 
     * Given a feature configuration, parses the features inside into a collection.
     * @param config is a configuration to be questioned.
     * @return collection of feature names in the given feature configuration.
     */
    private static Collection<String> parseFeatureConfig(String config) {
        Collection<String> features = new ArrayList<>();
        //String[] result = config.split("\\s+");
        String[] result = FamiliarConfigurationParser.parseConfig(config);
        for (int i = 0; i < result.length; i++) {
            features.add(result[i]); 
        }
        return features;
    }

    /**
     * Given a set of features, arranges the input string for familiar to check validity.
     * @param configuration is the set of features.
     * @return is the string to be given to familiar.
     */
    public String arrangeFeatureConfigurationString(Set<COREFeature> configuration) {
        String configString = "";
        for (COREFeature feat : configuration) {
            configString += arrangeFeatureName(feat) + " ";
        }
        return configString;
    }

    /**
     * Used for calculating the total calls to familiar.
     */
    public void getCountAndReset() {
        int copy = familiarCallCount;
        familiarCallCount = 0;
        int secondCopy = totalCount;
        totalCount = 0;
        System.out.println("Total Familiar calls : " + copy);
        System.out.println("Total call is : " + secondCopy);
    }

    /**
     * Used for a check of XOR conflict before asking familiar.
     * @param selected is the features to be selected.
     * @return true if there is a conflict in the set.
     */
    public boolean checkForXORConflicts(HashSet<COREFeature> selected) {
        
        List<COREFeature> conflicLlist = new ArrayList<>(selected);
        // Add all the possible parents
        for (COREFeature feature : selected) {
            COREFeature parent = feature.getParent();

            while (parent != null) {
                if (conflicLlist.contains(parent)) {
                    break;
                } else {
                    conflicLlist.add(parent);
                    parent = parent.getParent();
                }
            }
        }

        int i = 0;
        int j = 1;
        while (i < (conflicLlist.size() - 1)) {
            while (j < conflicLlist.size()) {
                if (conflicLlist.get(i).getParentRelationship() == COREFeatureRelationshipType.XOR) {
                    if (conflicLlist.get(j).getParentRelationship() == COREFeatureRelationshipType.XOR) {
                        if (conflicLlist.get(i).getParent() == conflicLlist.get(j).getParent()) {
                            return true;
                        } else {
                            j++;
                        }
                    } else {
                        j++;
                    }
                } else {
                    i++;
                    j = i + 1;
                }
            }
            i++;
            j = i + 1;
        }
        return false;
    }
}

package ca.mcgill.sel.core.evaluator.im;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import ca.mcgill.sel.core.COREContribution;
import ca.mcgill.sel.core.COREFeature;
import ca.mcgill.sel.core.COREFeatureImpactNode;
import ca.mcgill.sel.core.COREFeatureModel;
import ca.mcgill.sel.core.COREImpactModel;
import ca.mcgill.sel.core.COREImpactNode;
import ca.mcgill.sel.core.COREWeightedLink;

/**
 * Class that provides the scale factor and offset calculation capabilities.
 * @author berkduran
 *
 */
public final class ScaleFactorAndOffsetCalculator {

    private static Map<COREImpactNode, EvaluationInformation> maximumEvaluationTable = new HashMap<>();
    private static Map<COREImpactNode, EvaluationInformation> minimumEvaluationTable = new HashMap<>();
    private static Set<COREFeature> mandatoryFeatures = new HashSet<>();
    private static ImpactModelClusters impactModelClusters;
    
    /**
     * Constructor.
     */
    private ScaleFactorAndOffsetCalculator() {
    }

    /**
     * Initialization function.
     * @param impactModel is the impact model.
     */
    private static void initializeEvaluationTables(COREImpactModel impactModel) {
        for (COREImpactNode modelElement : impactModel.getImpactModelElements()) {
            // For goals, the table entries are set to default and will be calculated later.
            // For features, the satisfaction should be calculated considering the ones that have mappings.
            // Therefore, set the initial table entries for the features and calculate their scale factor and offset
            // values.
            if (modelElement instanceof COREFeatureImpactNode) {
                
                evaluateFeature((COREFeatureImpactNode) modelElement);
                
            } else {
                
                EvaluationInformation evalInfoForMax = new EvaluationInformation(true);
                EvaluationInformation evalInfoForMin = new EvaluationInformation(false);
                
                maximumEvaluationTable.put(modelElement, evalInfoForMax);
                minimumEvaluationTable.put(modelElement, evalInfoForMin);
            }
        }
    }
    
    /**
     * Backup function that sets scale factor to 1 and offset to zero.
     * @param impactModel is the impact model.
     * @param featureModel is the feature model.
     */
    public static void calculateScaleFactorsAndOffsets2(COREImpactModel impactModel, COREFeatureModel featureModel) {
        for (COREImpactNode element : impactModel.getImpactModelElements()) {
            element.setOffset(0.0f);
            element.setScalingFactor(1.0f);
        }
    }
    /**
     * Main function to calculate scale factors and offsets.
     * @param impactModel is the impact model.
     * @param featureModel is the feature model.
     */
    public static void calculateScaleFactorsAndOffsets(COREImpactModel impactModel, COREFeatureModel featureModel) {
        // Function initialization
        FamiliarHandler familiarHandler = new FamiliarHandler(featureModel);
        // Determine the mandatory features.
        mandatoryFeatures = ImpactModelEvaluationUtil.collectMandatoryChildren(featureModel.getRoot());
        // Divide impact model into  clusters to later determine the order of evaluation.
        impactModelClusters = new ImpactModelClusters(impactModel);
        // Initialize the max and min evaluation tables.
        initializeEvaluationTables(impactModel);
        
        // Set scale factors and offsets for goals.
        // First arrange the ready list and waiting lists so that the evaluation is done from bottom to top.
        // This ensures that for a goal to be evaluated, all of its contributors are already evaluated.
        List<COREImpactNode> goalsReadyToEvaluate = new ArrayList<>();
        List<COREImpactNode> goalsWaiting = new ArrayList<>();
        for (COREImpactNode aGoal : impactModelClusters.getEvalCount().keySet()) {
            if (impactModelClusters.getEvalCount().get(aGoal) == 0) {
                goalsReadyToEvaluate.add(aGoal);
            } else {
                goalsWaiting.add(aGoal);
            }
        }

        while (goalsReadyToEvaluate.size() > 0) {
            COREImpactNode goalToEvaluate = goalsReadyToEvaluate.get(0);
            goalsReadyToEvaluate.remove(goalToEvaluate);
            List<COREImpactNode> contributors = impactModelClusters.getClusters().get(goalToEvaluate);

//            System.out.println("===== Evaluating Cluster with Target: " + goalToEvaluate.getName());
            evaluateCluster(goalToEvaluate, contributors, familiarHandler);
//            System.out.println("===== Evaluation of Cluster with Target: " + goalToEvaluate.getName() 
//                    + " Completed with SF: " + goalToEvaluate.getScalingFactor() 
//                    + ", OF: " + goalToEvaluate.getOffset());

            //After evaluating a ready element, update the evaluation counter and wait list.
            for (COREContribution outgoingLink : goalToEvaluate.getOutgoing()) {
                COREImpactNode contributed = outgoingLink.getImpacts();
                int newCount = impactModelClusters.getEvalCount().get(contributed) - 1;
                impactModelClusters.getEvalCount().put(contributed, newCount);
                if (newCount == 0) {
                    goalsWaiting.remove(contributed);
                    goalsReadyToEvaluate.add(contributed);
                }
            }
        }
        
        impactModelClusters = null;
        goalsReadyToEvaluate = null;
        goalsWaiting = null;
        mandatoryFeatures.clear();
        maximumEvaluationTable.clear();
        minimumEvaluationTable.clear();
    }
    
    /**
     * Function to calculate feature evaluations.
     * @param aFeature is either regular features or feature impact nodes.
     */
    private static void evaluateFeature(COREFeatureImpactNode aFeature) {
        boolean isMandatory = mandatoryFeatures.contains(aFeature.getRepresents());
        float maxAchievable = 0.0f;
        float minAchievable = 0.0f;
        EvaluationInformation evalInfoForMax = new EvaluationInformation(true);
        EvaluationInformation evalInfoForMin = new EvaluationInformation(false);

        if (aFeature.getWeightedLinks().isEmpty()) {
            // Ordinary feature. Can be 100 (selected) or 0 (not selected).
            // If the feature is mandatory, only option is being selected.
            maxAchievable = ImpactModelEvaluationUtil.getMaxPossibleRelativeValue();
            if (isMandatory) {
                minAchievable = ImpactModelEvaluationUtil.getMaxPossibleRelativeValue();
            } else {
                minAchievable = ImpactModelEvaluationUtil.getMinPossibleRelativeValue();
            }
        } else {
            // Special node.
            int selfWeight = aFeature.getRelativeFeatureWeight();
            int maxWeight = selfWeight;
            int minWeight = selfWeight;
            for (COREWeightedLink aLink : aFeature.getWeightedLinks()) {
                if (aLink.getWeight() > 0) {
                    maxWeight += aLink.getWeight();
                } else {
                    minWeight += aLink.getWeight();
                }
            }
            maxAchievable = maxWeight;
            minAchievable = minWeight;
            // if the node is not mandatory and it's maximum is negative, the case when it's not selected yields max.
            if (!isMandatory && maxAchievable < 0) {
                maxAchievable = 0.0f;
            }
            // if the node is not mandatory and it's minimum is positive, the case when it's not selected yields min.
            if (!isMandatory && minAchievable > 0) {
                minAchievable = 0.0f;
            }
        }
        
        //Now add the entries for the evaluation tables for the feature at hand.
        EvaluationValueSet selectedMax = new EvaluationValueSet();
        selectedMax.addToConfiguration(aFeature.getRepresents());
        selectedMax.setIndex(new int[]{0});
        selectedMax.setValue(maxAchievable);
        evalInfoForMax.addToAchievableValueTable(selectedMax);
        evalInfoForMax.addToCalculatedIndexes(new int[]{0});
        evalInfoForMax.setLastCalculated(0);

        EvaluationValueSet selectedMin = new EvaluationValueSet();
        selectedMin.addToConfiguration(aFeature.getRepresents());
        selectedMin.setIndex(new int[]{0});
        selectedMin.setValue(maxAchievable);
        evalInfoForMin.addToAchievableValueTable(selectedMin);
        evalInfoForMin.addToCalculatedIndexes(new int[]{0});
        evalInfoForMin.setLastCalculated(0);
        // If the feature is mandatory, only option is being selected. 
        // Hence, no need to add the not selected option to the evaluation table.
        if (!isMandatory) {
            EvaluationValueSet notSelectedMax = new EvaluationValueSet();
            notSelectedMax.addToUnwanted(aFeature.getRepresents());
            notSelectedMax.setIndex(new int[]{1});
            notSelectedMax.setValue(minAchievable);
            evalInfoForMax.addToAchievableValueTable(notSelectedMax);
            evalInfoForMax.addToCalculatedIndexes(new int[]{1});
            evalInfoForMax.setLastCalculated(1);

            EvaluationValueSet notSelectedMin = new EvaluationValueSet();
            notSelectedMin.addToUnwanted(aFeature.getRepresents());
            notSelectedMin.setIndex(new int[]{1});
            notSelectedMin.setValue(minAchievable);
            evalInfoForMin.addToAchievableValueTable(notSelectedMin);
            evalInfoForMin.addToCalculatedIndexes(new int[]{1});
            evalInfoForMin.setLastCalculated(1);
        }
        
        maximumEvaluationTable.put(aFeature, evalInfoForMax);
        minimumEvaluationTable.put(aFeature, evalInfoForMin);
        
        ImpactModelEvaluationUtil.calculateAndSetScaleFactorAndOffsetValues(aFeature, maxAchievable, minAchievable);
    }

    /**
     * Function to evaluate a cluster that contains a goal and its contributors.
     * @param target is the goal.
     * @param contributors are the contributors to the targetGoal.
     * @param familiarHandler is used to check configuration validity.
     */
    private static void evaluateCluster(COREImpactNode target, List<COREImpactNode> contributors, 
            FamiliarHandler familiarHandler) {
        float maxAchievable = 0.0f;
        float minAchievable = 0.0f;

        //First, evaluate for the maximum.
//        System.out.println("===== Evaluating for max.");
        EvaluationValueSet trialSet = new EvaluationValueSet();
        boolean isConfigValid = false;
        int valueIndex = 0;
        while (!isConfigValid) {

            trialSet = getIndexedMaxEvaluationForElement(target, valueIndex);
            
            //make sure that configuration has at least the root.
            if (trialSet == null) {
                System.out.println("Trial Set does not exist for goal: " + target.getName()
                        + " for index: " + valueIndex);
            }

            isConfigValid = familiarHandler.checkConfigurationValidity(trialSet.getConfiguration(),
                    trialSet.getUnwanted());

            if (isConfigValid) {
                maxAchievable = trialSet.getValue();
                //now that we know the first index for which we have a valid configuration, save this.
                //so that the algorithm will not waste time going through the calculated invalid configurations,
                //while trying to calculate for the higher level goals.
                maximumEvaluationTable.get(target).setValidEvaluationIndex(valueIndex);
//                System.out.println("CONFIG IS VALID YEY!");
//                System.out.println("Max achievable value is: " + maxAchievable 
//                  + " for goal: " + targetGoal.getName());
            } else {
                valueIndex++;
//                System.out.println("CONFIG IS INVALID...");
            }
        }
//        System.out.println("===== Max is: " + maxAchievable);
        //calculatedMaxIndex = valueIndex;
        //combinationsAtMaxCalculation = goalSurrogateMapForMax.get(goal).getMap().get(featureModel.getRoot())
        // .getAchievableValueTable().size();

        //Now calculate the minimum.
//        System.out.println("===== Evaluating for min.");
        trialSet = new EvaluationValueSet();
        isConfigValid = false;
        valueIndex = 0;
        while (!isConfigValid) {
            trialSet = getIndexedMinEvaluationForElement(target, valueIndex);
            
            //make sure that configuration has at least the root.
            if (trialSet == null) {
                System.out.println("Trial Set does not exist for goal: " + target.getName()
                        + " for index: " + valueIndex);
            }

            isConfigValid = familiarHandler.checkConfigurationValidity(trialSet.getConfiguration(),
                    trialSet.getUnwanted());

            if (isConfigValid) {
                minAchievable = trialSet.getValue();
                //now that we know the first index for which we have a valid configuration, save this.
                //so that the algorithm will not waste time going through the calculated invalid configurations,
                //while trying to calculate for the higher level goals.
                minimumEvaluationTable.get(target).setValidEvaluationIndex(valueIndex);
//                System.out.println("CONFIG IS VALID YEY!");
//                System.out.println("Min achievable value is: " + minAchievable 
//                  + " for goal: " + targetGoal.getName());
            } else {
                valueIndex++;
//                System.out.println("CONFIG IS INVALID...");
            }
        }

//        System.out.println("===== Min is: " + minAchievable);
        //calculatedMinIndex = valueIndex;
        //combinationsAtMinCalculation = goalSurrogateMapForMin.get(goal).getMap().get(featureModel.getRoot())
        // .getAchievableValueTable().size();

        ImpactModelEvaluationUtil.calculateAndSetScaleFactorAndOffsetValues(target, maxAchievable, minAchievable);
    }
    
    /**
     * Function that is recursively called for a goal with different indexes to evaluate the max achievable value.
     * @param element 
     * @param index 
     * @return the evaluation information for that target goal.
     */
    private static EvaluationValueSet getIndexedMaxEvaluationForElement(COREImpactNode element, int index) {
        //the asked index will start from 0. However, if the element has evaluated some invalid configurations before,
        //we offset the asked index with the index of first valid configuration (if calculated) in order to not go 
        //through those invalid configurations once again.
        int checkIndex = index;
        if (maximumEvaluationTable.get(element).getValidEvaluationIndex() != -1) {
            checkIndex += maximumEvaluationTable.get(element).getValidEvaluationIndex();
        }
        //if the indexed value has already been calculated, return that.
        if (maximumEvaluationTable.get(element).getLastCalculated() >= checkIndex) {
            return maximumEvaluationTable.get(element).getAchievableValueTable().get(checkIndex);
        } else if (element instanceof COREFeatureImpactNode) {
            //if the element is a feature and an index greater than 1 is being asked, return null.
            return null;
        } 

        List<int[]> indexesToCheck = ImpactModelEvaluationUtil
                .calculateNextBestIndexesToCheck(element, maximumEvaluationTable.get(element));
        List<COREImpactNode> childrenToBeAsked = impactModelClusters.getClusters().get(element);

        for (int[] indexToAsk : indexesToCheck) {
            boolean indexAlreadyCalculated = ImpactModelEvaluationUtil.checkExistingCalculatedIndexes(indexToAsk, 
                    maximumEvaluationTable.get(element).getCalculatedIndexes());

            if (!indexAlreadyCalculated) {
                //ask the children.
                EvaluationValueSet evalForSelf = new EvaluationValueSet();
                Set<COREFeature> selfConfig = new HashSet<>();
                Set<COREFeature> selfUnwanted = new HashSet<>();
                float selfValue = 0.0f;
                boolean isEvaluationDone = true;
                for (int childNo = 0; childNo < indexToAsk.length; childNo++) {
                    EvaluationValueSet evalForChild = null;
                    COREImpactNode child = childrenToBeAsked.get(childNo);
                    //decide on whether to ask for max or min evaluation depending on the contribution.
                    float cont = ImpactModelEvaluationUtil.getWeightedContributionBetweenElements(child, element);
                    if (cont >= 0) {
                        evalForChild = getIndexedMaxEvaluationForElement(child, indexToAsk[childNo]);
                    } else {
                        evalForChild = getIndexedMinEvaluationForElement(child, indexToAsk[childNo]);
                    }
                    //combine and sum up.
                    if (evalForChild != null) {
                        selfConfig.addAll(evalForChild.getConfiguration());
                        selfUnwanted.addAll(evalForChild.getUnwanted());
                        //at this point, each satisfaction should be adjusted with 
                        //first: the scale factor and offset of the child,
                        //second: the corresponding contribution from child to parent,
                        selfValue += ImpactModelEvaluationUtil.scaleAndOffsetSatisfaction(evalForChild.getValue(),
                                child.getScalingFactor(), child.getOffset()) * cont;
                    } else {
                        isEvaluationDone = false;
                        break;
                    }
                }
                if (isEvaluationDone) {
                    evalForSelf.setIndex(indexToAsk);
                    evalForSelf.setConfiguration(selfConfig);
                    evalForSelf.setUnwanted(selfUnwanted);
                    evalForSelf.setValue(selfValue);
                    maximumEvaluationTable.get(element).addToAchievableValueTable(evalForSelf);
                    maximumEvaluationTable.get(element).addToCalculatedIndexes(indexToAsk);
                }
            }
        }

        EvaluationValueSet resultantEvaluation = null;
        if (maximumEvaluationTable.get(element).getAchievableValueTable().size() > checkIndex) {
            maximumEvaluationTable.get(element).setLastCalculated(checkIndex);
            resultantEvaluation = maximumEvaluationTable.get(element).getAchievableValueTable().get(checkIndex);
        } 
        return resultantEvaluation;
        
    }
    
    /**
     * Function that is recursively called for a goal with different indexes to evaluate the min achievable value.
     * @param element 
     * @param index 
     * @return the evaluation information for that target goal.
     */
    private static EvaluationValueSet getIndexedMinEvaluationForElement(COREImpactNode element, int index) {
        //the asked index will start from 0. However, if the element has evaluated some invalid configurations before,
        //we offset the asked index with the index of first valid configuration (if calculated) in order to not go 
        //through those invalid configurations once again.
        int checkIndex = index;
        if (minimumEvaluationTable.get(element).getValidEvaluationIndex() != -1) {
            checkIndex += minimumEvaluationTable.get(element).getValidEvaluationIndex();
        }
        //if the indexed value has already been calculated, return that.
        if (minimumEvaluationTable.get(element).getLastCalculated() >= checkIndex) {
            return minimumEvaluationTable.get(element).getAchievableValueTable().get(checkIndex);
        } else if (element instanceof COREFeatureImpactNode) {
            //if the element is a feature and an index greater than 1 is being asked, return null.
            return null;
        }
        
        List<int[]> indexesToCheck = ImpactModelEvaluationUtil
                .calculateNextBestIndexesToCheck(element, minimumEvaluationTable.get(element));
        List<COREImpactNode> childrenToBeAsked = impactModelClusters.getClusters().get(element);

        for (int[] indexToAsk : indexesToCheck) {
            boolean indexAlreadyCalculated = ImpactModelEvaluationUtil.checkExistingCalculatedIndexes(indexToAsk, 
                    minimumEvaluationTable.get(element).getCalculatedIndexes());

            if (!indexAlreadyCalculated) {
                //ask the children.
                EvaluationValueSet evalForSelf = new EvaluationValueSet();
                Set<COREFeature> selfConfig = new HashSet<>();
                Set<COREFeature> selfUnwanted = new HashSet<>();
                float selfValue = 0.0f;
                boolean isEvaluationDone = true;
                for (int childNo = 0; childNo < indexToAsk.length; childNo++) {
                    EvaluationValueSet evalForChild = null;
                    COREImpactNode child = childrenToBeAsked.get(childNo);
                    //decide on whether to ask for max or min evaluation depending on the contribution.
                    float cont = ImpactModelEvaluationUtil.getWeightedContributionBetweenElements(child, element);
                    if (cont >= 0) {
                        evalForChild = getIndexedMinEvaluationForElement(child, indexToAsk[childNo]);
                    } else {
                        evalForChild = getIndexedMaxEvaluationForElement(child, indexToAsk[childNo]);
                    }
                    //combine and sum up.
                    if (evalForChild != null) {
                        selfConfig.addAll(evalForChild.getConfiguration());
                        selfUnwanted.addAll(evalForChild.getUnwanted());
                        selfValue += ImpactModelEvaluationUtil.scaleAndOffsetSatisfaction(evalForChild.getValue(),
                                child.getScalingFactor(), child.getOffset()) * cont;
                    } else {
                        isEvaluationDone = false;
                        break;
                    }
                }
                if (isEvaluationDone) {
                    evalForSelf.setIndex(indexToAsk);
                    evalForSelf.setConfiguration(selfConfig);
                    evalForSelf.setUnwanted(selfUnwanted);
                    evalForSelf.setValue(selfValue);
                    minimumEvaluationTable.get(element).addToAchievableValueTable(evalForSelf);
                    minimumEvaluationTable.get(element).addToCalculatedIndexes(indexToAsk);
                }
            }
        }
        EvaluationValueSet resultantEvaluation = null;
        if (minimumEvaluationTable.get(element).getAchievableValueTable().size() > checkIndex) {
            minimumEvaluationTable.get(element).setLastCalculated(checkIndex);
            resultantEvaluation = minimumEvaluationTable.get(element).getAchievableValueTable().get(checkIndex);
        } 
        return resultantEvaluation;
    }
}

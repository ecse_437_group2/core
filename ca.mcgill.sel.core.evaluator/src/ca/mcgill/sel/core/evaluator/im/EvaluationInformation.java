package ca.mcgill.sel.core.evaluator.im;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * This class encapsulates the features with additional information necessary for the evaluation.
 * @author berkduran
 *
 */
public class EvaluationInformation {
    private int lastCalculated = -1;
    private Set<String> calculatedIndexes = new HashSet<>();
    private List<EvaluationValueSet> achievableValueTable = new ArrayList<>();
    private boolean sortForMax;
    private int validEvaluationIndex = -1;
    /**
     * Constructor.
     * @param sortForMax sets the sorting order. If true, the table is sorted from highest to lowest evaluation value. 
     * If false, the table is sorted from lowest to highest evaluation value.
     */
    protected EvaluationInformation(boolean sortForMax) {
        this.sortForMax = sortForMax;
    }
    /**
     * Getter for the table.
     * @return the table.
     */
    public List<EvaluationValueSet> getAchievableValueTable() {
        return achievableValueTable;
    }
    
    /**
     * Adder for the table.
     * @param valueSet is used to add to the table.
     */
    public void addToAchievableValueTable(EvaluationValueSet valueSet) {
        
        if (achievableValueTable.isEmpty()) {
            achievableValueTable.add(valueSet);
        } else {
            float valueToInsert = valueSet.getValue();
            for (int i = lastCalculated; i < achievableValueTable.size(); i++) {
                float valueToCheck = achievableValueTable.get(i).getValue();
                if ((sortForMax && (valueToInsert > valueToCheck)) 
                        || (!sortForMax && (valueToInsert < valueToCheck))) {
                    achievableValueTable.add(i, valueSet);
                    return;
                }
            }
            achievableValueTable.add(valueSet);
        }
    }
    
    /**
     * Getter for the last calculated value.
     * @return the index of last calculated value. 0 for max.
     */
    public int getLastCalculated() {
        return lastCalculated;
    }
    /**
     * Setter for last calculated value.
     * @param lastCalculated is obvious.
     */
    public void setLastCalculated(int lastCalculated) {
        this.lastCalculated = lastCalculated;
    }
    
    /**
     * Getter.
     * @return the validEvaluationIndex
     */
    public int getValidEvaluationIndex() {
        return validEvaluationIndex;
    }
    /**
     * Setter.
     * @param validEvaluationIndex the validEvaluationIndex to set
     */
    public void setValidEvaluationIndex(int validEvaluationIndex) {
        this.validEvaluationIndex = validEvaluationIndex;
    }
    /**
     * Getter for calculated indexes.
     * @return the map.
     */
    public Set<String> getCalculatedIndexes() {
        return calculatedIndexes;
    }
    
    /**
     * Used for adding a calculated index to the surrogate.
     * @param calculatedIndex 
     */
    public void addToCalculatedIndexes(int[] calculatedIndex) {
        String calculatedIndexText = "";
        for (int a : calculatedIndex) {
            calculatedIndexText += a + ",";
        }
        if (calculatedIndexText.length() > 1) {
            calculatedIndexText = calculatedIndexText.substring(0, calculatedIndexText.lastIndexOf(","));
        } 
        calculatedIndexes.add(calculatedIndexText);
    }
}
